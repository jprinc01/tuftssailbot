Tufts Robotic Sail Team: Sailbot 2013
=====================================

Max Goldstein, Dominic Guri

This repo contains the **tests** directory (folder), which contains brief
programs to isolate and test specific functionalities, and the **state-machine**
directory, which is the combination of all of these plus sailing logic that
will drive the boat.
