/*
    Max Goldstein
    Sailbot Ardunio
*/

#include <LSM303.h> // Direction Sensor
#include <Wire.h> // Direction Sensor
#include <SPI.h> // Teensy (Lon, lat, Dir Sensor Wind)
#include <Servo.h> // Servo

#include "Coordinate.h" //Custom geometry classes
#include "Distance.h"
#include "BoatControl.h"

// Compass variable
LSM303 compass;

// Control instance
BoatControl control;

//my location, the target location, and the tack waypoint
Coordinate my, go, way;

Distance togo(){
  return Distance(my, go);
}

int tack = 0; // counts the tack you are doing
const int BEARING = 0.87266; //angle between heading and wind, in radians (50 deg)

// SPI => teensy variables
boolean lockedOn = false;
String received = "";

// Used for parsing teensy input data
byte ReadByte(void) {
  while(!(SPSR & (1<<SPIF))) ;
  return SPDR;
}

void setup()
{
    Serial.begin(9600);
// Compass Setup
    Wire.begin();
    compass.init();
    compass.enableDefault();
    // Calibration values. Use the Calibrate example program to get the values for
    // your compass. MIGHT HAVE TO CHANGE BASED ^
    compass.m_min.x = -520; compass.m_min.y = -570; compass.m_min.z = -770;
    compass.m_max.x = +540; compass.m_max.y = +500; compass.m_max.z = 180;
// SPI Setup
    SPI.begin();
    SPCR = B00000000;
    SPCR = (1<<SPE);
}

void readSPI(){
    byte rxData;
    rxData = ReadByte();
    if (rxData == ';'){
        if (! lockedOn){
            lockedOn = true;
            received = "";
        }else{
            parseSPI();
            Serial.println(received+";");
            received = "";
        }
    }else if (lockedOn){
        received += (char) rxData;
    }
}

void parseSPI(){
    int comma = received.indexOf(',');
    float mylat = received.substring(0,comma).toInt()/100000.;
    int semicolon = received.indexOf(';');
    float mylon = received.substring(comma+1,semicolon).toInt()/100000.;
    my = Coordinate(mylat, mylon);
    if (go.lat == 0){ //test with a coordiante 50 ft north of us
        go = Coordinate(mylat+.00014, mylon);
        newTarget();
    }
}

void newTarget(){
  tack = 1;
  Coordinate midpoint = goto().midpoint();
  Distance upper = Distance(midpoint, go);
  way = Coordinate(upper.lon, -1*upper.lat); //reverse order to rotate 90 deg
                                             //should be clockwise but haven't tested
}

void loop()
{
    if (control.readManualData()){
        control.ManualControl();
    }else{
        readSPI();
        //test for boundaries here
    }

}

