#include "Arduino.h"
#include "Coordinate.h"
#include "Distance.h"

Distance::Distance(Coordinate from, Coordinate to){
  base = Coordinate(from.lat, from.lon);
  dlat = to.lat - from.lat;
  dlon = to.lon - from.lon;
}

float Distance::magnitude(){
  return sqrt(dlat*dlat + dlon*dlon);
}

Coordinate Distance::midpoint(){
  return Coordinate(base.lat+dlat/2., base.lon+dlon/2.);
}
