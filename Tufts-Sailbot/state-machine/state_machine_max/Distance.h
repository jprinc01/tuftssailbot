class Distance{
  public:
  Coordinate base;
  float dlat, dlon;

  Distance(Coordinate from, Coordinate to);
  float magnitude();
  Coordinate midpoint();
};
