/*
   Dominic Guri

Libraries:
- LSM303 => Direction Sensor
- Wire   => Direction Sensor
- SPI    => Teensy (Lon, lat, Dir Sensor Wind)
- Servo.h

For use on the Sailbot Arduino

 */
#include <LSM303.h>
#include <Wire.h>
#include <SPI.h>
#include <Servo.h>
#include <math.h>

#define MAX_PULSE 25000
#define MAX_ANGLE 3000

// Compass variables
LSM303 compass;

// Manual control - Channels, Pin, and Servo Variables
boolean ManControl = true;
int boomChannel;
int rudderChannel;

int boomPin = 7;
int rudderPin = 5;
int pot = A0;

Servo boomServo;
Servo rudderServo;

// SPI => teensy variables
boolean lockedOn = false;
String received = "";

float mylat = 0;
float mylon = 0;
float lat_dest = mylat;
float lon_dest = mylon;
int boatDir = 0;
int windDir = 0;
int windAngle = 0;

void setup()
{
  // Compass Setup
  Wire.begin();
  compass.init();
  compass.enableDefault();
  // Calibration values. Use the Calibrate example program to get the values for
  // your compass. MIGHT HAVE TO CHANGE BASED ^
  compass.m_min.x = -520; compass.m_min.y = -570; compass.m_min.z = -770;
  compass.m_max.x = +540; compass.m_max.y = +500; compass.m_max.z = 180;
  // Manual Control Setup
  boomServo.attach(9);
  rudderServo.attach(8);

  pinMode(rudderPin, INPUT);
  pinMode(boomPin, INPUT);
  // SPI Setup
  SPI.begin();
  Serial.begin(9600);
  SPCR = B00000000;
  SPCR = (1<<SPE);
  // Potentiometer
  pinMode(pot, INPUT);
}

// Used for parsing teensy input data
byte ReadByte(void) {
  while(!(SPSR & (1<<SPIF))) ;
  return SPDR;
}

// reads in the data signals from the RC Control
// if the RC Control is OFF, boomChannel & rudderChannel
// will equal ZERO and ManControl will equal TRUE
void readManualData()
{
  boomChannel = pulseIn(boomPin, HIGH, MAX_PULSE);
  rudderChannel = pulseIn(rudderPin, HIGH, MAX_PULSE);

  if (boomChannel == 0 && rudderChannel == 0)
  {
    ManControl = true;
  }
  else
  {
    ManControl = false;
  }
}

// ManualControl uses writeMicroseconds()
void ManualControl()
{
  rudderServo.writeMicroseconds(MAX_ANGLE - rudderChannel);
  delay(15);
  boomServo.writeMicroseconds(boomChannel);
  delay(15);
}

void loop()
{
  readManualData();
  if (ManControl)
  {
    ManualControl();
  }
  readSPI();
  windAngle = getWindAngle();
  trimBoom(windAngle);

  tackToPoint(mylat, mylon, lat_dest, lon_dest); // This will only tack if required 
}

int getWindAngle() {
  windAngle = analogRead(pot);
  windAngle %= 100;
  return map(windAngle, 0, 99, 0, 359);
}

void readSPI() {
  byte rxData;
  rxData = ReadByte();
  if (rxData == ';') {
    if (!lockedOn) {
      lockedOn = true;
      received = "";
    }else {
      parseSPI();
      Serial.println(received+";");
      received = "";
    }
  }else if (lockedOn) {
    received += (char) rxData;
  }
}

void parseSPI() {
  int comma1 = received.indexOf(',');
  mylat = received.substring(0,comma1).toInt()/100000.;
  int comma2 = received.indexOf(',');
  mylon = received.substring(comma1+1,comma2).toInt()/100000.;
  int semicolon = received.indexOf(';');
  boatDir = received.substring(comma2+1,semicolon).toInt();
  Serial.print(mylat,5);
  Serial.print(" ");
  Serial.print(mylon,5);
  Serial.print(" ");
  Serial.println(boatDir);
}

void serialEvent() {
  recordDestination();
}

// Store destination lat and lng from input data
void recordDestination() {
  int comma = received.indexOf(',');
  lat_dest = received.substring(0,comma).toInt()/100000.;
  int semicolon = received.indexOf(';'); 
  lon_dest = received.substring(comma+1,semicolon).toInt()/100000.;
}

// Tack the ship to a given GPS point
void tackToPoint(float lat_src, float lng_src, float lat_dest, float lng_dest) {
  // if no-go zone, then write to rudderServo (no more than 30 degrees one way or another)
  // else, don't tack
  double rotation = angleBetweenPoints(lat_src, lng_src, lat_dest, lng_dest);
  if (inNoGoZone(rotation)) {
    int turn = (int) rotation * 1500; // 1500 useconds == 90 degrees
    rudderServo.writeMicroseconds(turn);
  } else {
    rudderServo.writeMicroseconds(1500); // Directly ahead
  }
}

// No Go Zone is defined here as 45 degrees to 135 degrees where 0 degrees is absolute right
bool inNoGoZone(double rot) {
  if (rot > 45 && rot < 135) {
    return true;
  }
  return false;
}

// Returns angle in degrees
double angleBetweenPoints(float lt_s, float ln_s, float lt_d, float ln_d) {
  return (int) acos((lt_d - lt_s) / (ln_d - ln_s)) * 180 / M_PI;
}

// Using rotation, trim the boom according to the wind direction
// 0 degrees is fully trimmed, 90 degrees is no trim
void trimBoom(int rotation) {
  if(rotation > 90) {
    rotation = 90;
  } else if (rotation < 0) {
    rotation = 0;
  }

  int turn = rotation * 1500; // 1500 useconds == 90 degrees
  boomServo.writeMicroseconds(turn);
}
