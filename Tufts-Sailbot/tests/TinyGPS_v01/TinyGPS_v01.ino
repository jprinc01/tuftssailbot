#include <TinyGPS.h>
#include <SoftwareSerial.h>

#define RXPIN 2
#define TXPIN 3
#define GPSBAUD 4800

TinyGPS gps;
SoftwareSerial uart_gps(RXPIN, TXPIN);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  uart_gps.begin(GPSBAUD);
}

void loop() {
  // put your main code here, to run repeatedly: 
  while(uart_gps.available())     
  {

    int c = uart_gps.read(); 
    //Serial.println(c);   
    if(gps.encode(c))      
    {
      getgps(gps);         
    }
  }
}




void getgps(TinyGPS &gps)
{
  float latitude, longitude;
  //int year;
  //byte month, day, hour, minute, second, hundredths;
  gps.f_get_position(&latitude, &longitude);
  Serial.print(latitude, 5);
  Serial.print(longitude, 5);
  //gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths);
  //Serial.print(year);
  //Serial.print(hour, DEC);
  //Serial.print(minute, DEC);
}




