//Correctly parses Teensy’s output stream

#include <SPI.h>

boolean lockedOn = false;
String received = "";

float mylat, mylon;

void setup() {
    SPI.begin();
    SPCR = B00000000;
    SPCR = (1<<SPE);
    Serial.begin(9600);
}

void loop() {
    readSPI();
}

void readSPI(){
    byte rxData;
    rxData = ReadByte();
    if (rxData == ';'){
        if (! lockedOn){
            lockedOn = true;
            received = "";
        }else{
            parseSPI();
            Serial.println(received+";");
            received = "";
        }
    }else if (lockedOn){
        received += (char) rxData;
    }
}

void parseSPI(){
    int comma = received.indexOf(',');
    mylat = received.substring(0,comma).toInt()/100000.;
    int semicolon = received.indexOf(';');
    mylon = received.substring(comma+1,semicolon).toInt()/100000.;
    Serial.print(mylat,5);
    Serial.print(" ");
    Serial.println(mylon,5);
}

byte ReadByte(void) {
    while(!(SPSR & (1<<SPIF))) ;
    return SPDR;
}

