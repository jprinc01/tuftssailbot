/*
	Dominic Guri
	06/02/2013		8:13 PM (ATO)
	boom-rudder-control.ino
    formerly ArduinoTest.ino
	
		- Used to intercept the 
				Radio Control -> Receiver -> Servo
			communication chain to 
				Radio Control -> Receiver -> Arduino -> Servo
			communication chain
		- This proves that arduino can be used as a NODE
		which determines the control data sent to the servos
		(i.e. whether it's AUTONOMOUS or MANUAL CONTROL)
		- At this point, the AUTONOMOUS control will occur when
		the RADIO CONTROL is OFF so that
			boomChannel = 0
			rudderChannel = 0
*/

#include <Servo.h>

#define MAX_PULSE 25000
#define MAX_ANGLE 3000
//#define BOOM_SET 1680

int boomChannel;
int rudderChannel;

int boomPin = 7;
int rudderPin = 5;

Servo boomServo;
Servo rudderServo;

void setup()
{
	Serial.begin(9600);

	boomServo.attach(9);
	rudderServo.attach(8);

	pinMode(rudderPin, INPUT);
	pinMode(boomPin, INPUT);
}

void loop()
{
	boomChannel = pulseIn(boomPin, HIGH, MAX_PULSE);
	rudderChannel = pulseIn(rudderPin, HIGH, MAX_PULSE);
	Serial.print("boomChannel :");
	Serial.println(boomChannel);
	Serial.print("rudderChannel :");
	Serial.println(rudderChannel);

	rudderServo.writeMicroseconds(MAX_ANGLE - rudderChannel);
	delay(500);
	boomServo.writeMicroseconds(boomChannel);
	delay(500);
}
