#include "Arduino.h"
#include <Servo.h>
#include "ServoControl.cpp"

ServoControl::ServoControl(int ch)
{
	channel = ch;
}

ServoControl::~ServoControl()
{
	;
}

void ServoControl::attach(int pin)
{
	myServo.attach(pin);
}

void ServoControl::boom(int)
{
	mapBoom(int);
}

void ServoControl::rudder(int channelPulse)
{
	mapRudder(channelPulse);
}

//////////////////////////////////private///////////////////////////////
void ServoControl::mapRudder(int channelPulse)
{
	myServo.writeMicroseconds(3000 - channelPulse);
}

void ServoControl::mapBoom(int)
{
	myServo.writeMicroseconds(channelPulse);;
}




