#include <ServoControl.h>

int ch1; // pin 2
int ch2; // pin 4

ServoControl boom;

void setup() {

  pinMode(2, INPUT); // Set our input pins as such
  pinMode(4, INPUT);
  boom.attach(10);

  Serial.begin(9600); // Pour a bowl of Serial

}

void loop() {

  ch1 = pulseIn(2, HIGH, 25000); // Read the pulse width of 
  ch2 = pulseIn(4, HIGH, 25000); // each channel

  Serial.print("Channel 1:"); // Print the value of 
  Serial.println(ch1);        // each channel

  Serial.print("Channel 2:");
  Serial.println(ch2);
  
  myServo.writeMicroseconds(3000 - ch1);

  delay(1000); // I put this here just to make the terminal 
              // window happier
}
