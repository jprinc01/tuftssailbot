#ifndef SERVOCONTROL_H
#define SERVOCONTROL_H

#include "Arduino.h"
#include <Servo.h>
#include "ServoControl.h"

#define RUDDER_DEFAULT 1500   //writeMicroseconds(CENTER)

Servo myServo;


class ServoControl
{
    public:
    ServoControl(int);
    ~ServoControl();
    
    void attach(int);
    void boom(int);
    void rudder(int);
    
    private:
    int channel;

    void mapRudder(int); // converts pulseIn() int writeMicro_ int
    void mapBoom(int);  // converts pulseIn() int writeMicro_ int
};

#endif