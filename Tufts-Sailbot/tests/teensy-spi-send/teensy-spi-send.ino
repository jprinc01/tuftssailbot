//Teensy code
//SPI and GPS are working
//Need to add compass, other sensors

#include <SPI.h>
#include <TinyGPS.h>
#include <SoftwareSerial.h>

//SPI stuff
const int SPIDELAY = 10;
int spiIndex = 0;
String spiString = "";

void WriteByte(byte value) {
  SPDR = value;
  while (!(SPSR & (1<<SPIF))) ;
  return;
}

//GPS stuff
unsigned long fix_age;
long  lat, lon;
SoftwareSerial GPS(4,7);
TinyGPS gps;
void gpsdump(TinyGPS &gps);
bool feedgps();
void getGPS();

String getGPSstring(){
  // retrieves +/- lat/long in 100000ths of a degree
  gps.get_position(&lat, &lon, &fix_age);
  getGPS();
  return String(lat)+","+String(lon);
}

//Main functions

void setup(){
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV8);
  GPS.begin(9600);
  Serial.begin(9600);
  spiString = getSPIstring();
}

void loop(){
  sendSPI();
  delay(SPIDELAY);
}

//SPI string functions

String getSPIstring(){
  return getGPSstring()+";";
}

void sendSPI(){
  WriteByte(spiString.charAt(spiIndex));
  spiIndex++;
  if (spiIndex >= spiString.length()){
    spiString = getSPIstring();
    spiIndex = 0;
  } 
}

//Auxillary SGPS functions
//We're not sure how they work but they do
void getGPS(){
  bool newdata = false;
  unsigned long start = millis();
  // Every 1 seconds we print an update
  while (millis() - start < 1000)
  {
    if (feedgps ()){
      newdata = true;
    }
  }
  if (newdata)
  {
    gpsdump(gps);
  }
}
bool feedgps(){
  while (GPS.available())
  {
    if (gps.encode(GPS.read()))
      return true;
  }
  return false;
}
void gpsdump(TinyGPS &gps)
{
  //byte month, day, hour, minute, second, hundredths;
  gps.get_position(&lat, &lon, &fix_age);
  {
    feedgps(); // If we don't feed the gps during this long 
    //routine, we may drop characters and get checksum errors
  }
}



